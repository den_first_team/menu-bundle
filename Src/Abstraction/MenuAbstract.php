<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 15.02.2019
 * Time: 14:48
 */

namespace Mcore\MenuBundle\Abstraction;



use Mcore\MenuBundle\Interfaces\MenuInterface;
use Mcore\MenuBundle\Interfaces\MenuPointInterface;
use Mcore\MenuBundle\Exceptions\MenuPointKeyException;
use Mcore\MenuBundle\Services\MenuHandlerStub;

abstract class MenuAbstract implements MenuInterface
{

    private $points = [];

    /**
     * @param MenuPointInterface $point
     * @param null|string $id
     * @param null|string $parent
     * @throws MenuPointKeyException
     */
    public function addPoint(MenuPointInterface $point, ?string $id = null, ?string $parent = null): void
    {

        foreach ([$id, $point->getDefaultID(), get_class($point)] as $_id){
            if ($_id){
                $id = $_id; break;
            }
        }

        foreach ([$parent, $point->getDefaultParent(), MenuInterface::ROOT_PARENT] as $_parent){
            if ($_parent){
                $parent = $_parent; break;
            }
        }

        if (array_key_exists($id, $this->points)) {
            throw new MenuPointKeyException('Attempt to add a menu item with an existing id');
        }

        $this->points[$id] = [
            'parent' => $parent,
            'point' => $point
        ];
    }

    /**
     * @param string $id
     * @return MenuPointInterface|null
     */
    public function getPoint(string $id): ?MenuPointInterface
    {
        if (array_key_exists($id, $this->points)) {
            return $this->points[$id]['point'];
        }
        return null;
    }

    /**
     * @return array
     */
    public function getPoints(): array
    {
        return $this->points;
    }
}