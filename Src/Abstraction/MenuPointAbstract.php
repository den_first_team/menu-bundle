<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 15.02.2019
 * Time: 15:09
 */

namespace Mcore\MenuBundle\Abstraction;

use Mcore\MenuBundle\Interfaces\MenuPointInterface;

abstract class MenuPointAbstract implements MenuPointInterface
{

    /**
     * @var bool
     */
    private $active = false;

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return null;
    }

    /**
     * @return array|null
     */
    public function getRouteParams(): ?array
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    public function active(): void
    {
        $this->active = true;
    }

    public function inactive(): void
    {
        $this->active = false;
    }
}