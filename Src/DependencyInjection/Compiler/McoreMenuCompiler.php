<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 22.02.2019
 * Time: 12:45
 */

namespace Mcore\MenuBundle\DependencyInjection\Compiler;



use Mcore\MenuBundle\Interfaces\MenuInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class McoreMenuCompiler implements CompilerPassInterface
{

    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {

        if (!$menuDefinitionIds = $container->findTaggedServiceIds(MenuInterface::TAG)) {
            return;
        }

        foreach (array_keys($menuDefinitionIds) as $menuDefinitionId) {
            $definition = $container->findDefinition($menuDefinitionId);
            if ($menuPointDefinitionIds = $container->findTaggedServiceIds($menuDefinitionId::pointTag())) {
                foreach (array_keys($menuPointDefinitionIds) as $menuPointDefinitionId) {
                    $definition->addMethodCall('addPoint', [new Reference($menuPointDefinitionId)]);
                }
            }
         }

    }
}