<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 25.02.2019
 * Time: 13:19
 */

namespace Mcore\MenuBundle\Interfaces;

interface AdminMenuPointInterface extends MenuPointInterface
{
    const TAG = 'mcore.menu_set_bundle.admin_menu_point';
}