<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 25.02.2019
 * Time: 13:13
 */

namespace Mcore\MenuBundle\Interfaces;

interface FrontMenuPointInterface extends MenuPointInterface
{
    const TAG = 'mcore.menu_set_bundle.front_menu_point';
}