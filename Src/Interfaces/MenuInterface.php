<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 15.02.2019
 * Time: 14:20
 */

namespace Mcore\MenuBundle\Interfaces;


interface MenuInterface
{

    const TAG = 'mcore.menu_bundle.menu';

    const ROOT_PARENT = 'root';
    /**
     * @param MenuPointInterface $point
     * @param string $id
     * @param string $parent
     * $point MenuPointInterface::class
     */
    public function addPoint(MenuPointInterface $point, string $id, string $parent): void;

    /**
     * @param string $id
     * @return MenuPointInterface|null
     */
    public function getPoint(string $id): ?MenuPointInterface;

    /**
     * @return array
     */
    public function getPoints(): array;

    /**
     * @return string
     */
    public static function pointTag(): string;

}