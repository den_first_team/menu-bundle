<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 15.02.2019
 * Time: 14:27
 */

namespace Mcore\MenuBundle\Interfaces;


interface MenuPointInterface
{

    /**
     * @return null|string
     */
    public function getDefaultID(): ?string;

    /**
     * @return null|string
     */
    public function getDefaultParent(): ?string;

    /**
     * @return null|string
     */
    public function getLabel(): ?string;

}