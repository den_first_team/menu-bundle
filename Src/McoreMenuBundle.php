<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 19.02.2019
 * Time: 10:20
 */

namespace Mcore\MenuBundle;


use Mcore\MenuBundle\Interfaces\AdminMenuPointInterface;
use Mcore\MenuBundle\Interfaces\AdminSettingsMenuPointInterface;
use Mcore\MenuBundle\Interfaces\AdminSidebarPointInterface;
use Mcore\MenuBundle\Interfaces\FrontMenuPointInterface;
use Mcore\MenuBundle\Interfaces\FrontSidebarPointInterface;
use Mcore\MenuBundle\Interfaces\MenuInterface;
use Mcore\MenuBundle\DependencyInjection\Compiler\McoreMenuCompiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class McoreMenuBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $pointInterfaces = [
            AdminMenuPointInterface::class,
            FrontMenuPointInterface::class,
        ];
        foreach ($pointInterfaces as $interface){
            $container->registerForAutoconfiguration($interface)->addTag($interface::TAG);
        }
        $container->registerForAutoconfiguration(MenuInterface::class)->addTag(MenuInterface::TAG);
        $container->addCompilerPass(new McoreMenuCompiler());
    }

}