<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 25.02.2019
 * Time: 13:24
 */

namespace Mcore\MenuBundle\Services;


use Mcore\MenuBundle\Abstraction\MenuAbstract;
use Mcore\MenuBundle\Interfaces\AdminMenuPointInterface;


class AdminMenu extends MenuAbstract
{

    public static function pointTag(): string
    {
        return AdminMenuPointInterface::TAG;
    }
}