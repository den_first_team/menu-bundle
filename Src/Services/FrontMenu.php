<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 26.02.2019
 * Time: 12:31
 */

namespace Mcore\MenuBundle\Services;


use Mcore\MenuBundle\Abstraction\MenuAbstract;
use Mcore\MenuBundle\Interfaces\FrontMenuPointInterface;

class FrontMenu extends MenuAbstract
{

    public static function pointTag(): string
    {
        return FrontMenuPointInterface::TAG;
    }
}