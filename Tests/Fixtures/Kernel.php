<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 26.02.2019
 * Time: 16:44
 */

namespace Mcore\MenuBundle\Tests\Fixtures;


use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Kernel extends \Symfony\Component\HttpKernel\Kernel
{
    use MicroKernelTrait;

    const CONFIG_EXTS = '.{php,xml,yaml,yml}';
    /**
     * Returns an array of bundles to register.
     *
     * @return iterable|BundleInterface[] An iterable of bundle instances
     */
    public function registerBundles()
    {
        $contents = require 'Resources/config/bundles.php';
        foreach ($contents as $class => $envs) {
            yield new $class();
        }
    }

    /**
     * Add or import routes into your application.
     *
     *     $routes->import('config/routing.yml');
     *     $routes->add('/admin', 'App\Controller\AdminController::dashboard', 'admin_dashboard');
     *
     * @param RouteCollectionBuilder $routes
     */
    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        // TODO: Implement configureRoutes() method.
    }

    /**
     * @param ContainerBuilder $container
     * @param LoaderInterface $loader
     * @throws \Exception
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->addResource(new FileResource(__DIR__.'/Resources/config/bundles.php'));
        $container->setParameter('kernel.secret','test');
        $loader->load(__DIR__.'/Resources/config/{services}'.self::CONFIG_EXTS, 'glob');
        $loader->load(__DIR__.'/Resources/config/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
    }

    public function getContainerBuilderPublic()
    {
        return parent::getContainerBuilder(); // TODO: Change the autogenerated stub
    }
}