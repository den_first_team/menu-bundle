<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 27.02.2019
 * Time: 10:17
 */

namespace Mcore\MenuBundle\Tests\Fixtures\Services;


use Mcore\MenuBundle\Abstraction\MenuPointAbstract;
use Mcore\MenuBundle\Interfaces\AdminMenuPointInterface;

class AdminMenuPointTest extends MenuPointAbstract implements AdminMenuPointInterface
{

    /**
     * @return null|string
     */
    public function getDefaultID(): ?string
    {
        return 'admin_menu_point';
    }

    /**
     * @return null|string
     */
    public function getDefaultParent(): ?string
    {
        return null;
    }

    /**
     * @return null|string
     */
    public function getLabel(): ?string
    {
        return 'admin menu point';
    }

    /**
     * @return null|string
     */
    public function getRouteName(): ?string
    {
        // TODO: Implement getRouteName() method.
    }
}