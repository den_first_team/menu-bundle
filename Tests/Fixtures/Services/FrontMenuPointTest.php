<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 27.02.2019
 * Time: 12:40
 */

namespace Mcore\MenuBundle\Tests\Fixtures\Services;


use Mcore\MenuBundle\Abstraction\MenuPointAbstract;
use Mcore\MenuBundle\Interfaces\FrontMenuPointInterface;

class FrontMenuPointTest extends MenuPointAbstract implements FrontMenuPointInterface
{

    /**
     * @return null|string
     */
    public function getDefaultID(): ?string
    {
        return 'front_menu_point';
    }

    /**
     * @return null|string
     */
    public function getDefaultParent(): ?string
    {
        return null;
    }

    /**
     * @return null|string
     */
    public function getLabel(): ?string
    {
        // TODO: Implement getLabel() method.
    }

    /**
     * @return null|string
     */
    public function getRouteName(): ?string
    {
        // TODO: Implement getRouteName() method.
    }
}