<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 19.02.2019
 * Time: 12:48
 */

namespace Mcore\MenuBundle\Tests;

use Mcore\MenuBundle\Interfaces\MenuPointInterface;
use Mcore\MenuBundle\Abstraction\MenuAbstract;
use PHPUnit\Framework\TestCase;



class MenuTest extends TestCase
{

    /**
     * @var MenuAbstract
     */
    protected $fixture;

    protected $data;

    protected function setUp(): void
    {
        $this->fixture = $this->getMockBuilder(MenuAbstract::class)->getMockForAbstractClass();
        $this->data = [
            [$this->createMock(MenuPointInterface::class), 'id_value', 'id_parent'],
            [$this->createMock(MenuPointInterface::class), 'id_value1', 'id_parent1'],
            [$this->createMock(MenuPointInterface::class), 'id_value2', 'id_parent2'],
            [$this->createMock(MenuPointInterface::class), 'id_value3', 'id_parent3'],
            [$this->createMock(MenuPointInterface::class), 'id_value4', 'id_parent1'],
            [$this->createMock(MenuPointInterface::class), 'id_value5', 'id_parent1'],
            [$this->createMock(MenuPointInterface::class), 'id_value6', 'id_parent2'],
            [$this->createMock(MenuPointInterface::class), 'id_value7', 'id_parent2'],
            [$this->createMock(MenuPointInterface::class), 'id_value8', 'id_parent3'],
            [$this->createMock(MenuPointInterface::class), 'id_value9', 'id_parent3'],
            [$this->createMock(MenuPointInterface::class), 'id_value10', 'id_parent'],
        ];
    }

    /**
     * @return MenuAbstract
     * @throws \Mcore\MenuBundle\Exceptions\MenuPointKeyException
     */
    public function testAddPoint()
    {
        foreach ($this->data as $item) {
            $this->fixture->addPoint(...$item);
            $this->assertTrue(true);
        }
        return $this->fixture;
    }

    /**
     * @param MenuAbstract $fixture
     * @depends testAddPoint
     */
    public function testGetPoint(MenuAbstract $fixture)
    {
        foreach ($this->data as $item){
            $point = $fixture->getPoint($item[1]);
            $this->assertEquals($point, $item[0]);
        }
    }


    /**
     * @param MenuAbstract $fixture
     * @depends testAddPoint
     */
    public function testGetPoints(MenuAbstract $fixture)
    {
        $this->assertEquals($fixture->getPoints(), $this->getTestData()['points']);
    }



    public function getTestData()
    {
        $result = [];
        foreach ($this->data as $item) {
            [$point, $id, $parent] = $item;
            $result['points_add'][] = $point;
            $result['points'][$id] = [
                'parent' => $parent,
                'point' => $point
            ];
        }
        return $result;
    }
}
