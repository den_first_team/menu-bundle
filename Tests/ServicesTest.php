<?php
/**
 * Created by PhpStorm.
 * User: programmer_5
 * Date: 26.02.2019
 * Time: 15:01
 */

namespace Mcore\MenuBundle\Tests;


use Mcore\MenuBundle\Interfaces\AdminMenuPointInterface;
use Mcore\MenuBundle\Interfaces\FrontMenuPointInterface;
use Mcore\MenuBundle\Services\AdminMenu;
use Mcore\MenuBundle\Services\FrontMenu;
use Mcore\MenuBundle\Tests\Fixtures\Services\AdminMenuPointTest;
use Mcore\MenuBundle\Tests\Fixtures\Services\FrontMenuPointTest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServicesTest extends WebTestCase
{

    public function setUp() : void
    {
        self::bootKernel();
    }


    public function testMenuCompiler() : void {

        foreach ($this->menuServices() as $item){
            [$menuService, $menuPointInterface, $menuPoint] = $item;
            $service = static::$container->get($menuService);
            $servicePoint = self::$container->get($menuPoint);
            $this->assertEquals($service->getPoint($servicePoint->getDefaultID()),$servicePoint);
        }
    }

    protected function menuServices() : array {
        return [
            [AdminMenu::class, AdminMenuPointInterface::class, AdminMenuPointTest::class],
            [FrontMenu::class, FrontMenuPointInterface::class, FrontMenuPointTest::class]
        ];
    }

}